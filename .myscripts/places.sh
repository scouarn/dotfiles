# places.sh (this file is intended to be sourced and not executed)
#
# Directory bookmarks (cd with a fuzzy finder)
#
# Usage:
# places                Prompt and cd
# places_prompt         Prompt and echo the selected path
# places_add [<path>]   Add to the list of paths, defaults to the current working directory
# places_rm             Prompt and remove from the list
#
# scouarn 01 Apr 2023
#

if [ -z "$PLACES_LIST" ]; then
    PLACES_LIST="$HOME/.myscripts/places.list"
fi

if [ ! -f "$PLACES_LIST" ]; then
    touch "$PLACES_LIST"
fi

if [ -z "$PLACES_MENU" ]; then
    PLACES_MENU="fzf -q"
fi

places() { # $1: initial query
    DIR=$(places_prompt "$1")
    [ -d "$DIR" ] && cd "$DIR"
}

places_prompt() { # $1: initial query
    cat "$PLACES_LIST" | $PLACES_MENU "$1"
}

places_add() {
    if [ -z "$1" ]; then
        DIR='.'
    elif [ -d "$1" ]; then
        DIR="$1"
    else
        echo "Invalid directory: $1" >&2
    fi

    DIR=$(realpath "$DIR")

    # Add and remove duplicates
    echo "$DIR" >> "$PLACES_LIST"
    sort -u "$PLACES_LIST" -o "$PLACES_LIST"
}

places_rm() {
    cat "$PLACES_LIST" | grep -vxF "$(places_prompt)" > "$PLACES_LIST"
}
